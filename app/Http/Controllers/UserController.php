<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\Http\actions\UserFacade;

class UserController extends Controller
{
    public function login(Request $request)
    {
        return UserFacade::login($request);
    }

    public function register(Request $request)
    {
        return UserFacade::register($request);
    }
}
