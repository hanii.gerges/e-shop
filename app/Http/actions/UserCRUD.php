<?php

namespace App\Http\actions;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;


class UserCRUD
{

    public function login(Request $request)
    {
        //request->all() if u want to retrieve all input
        $credentials = $request->only('email','password');
        // attempt($credentials) // returns encoded token | false | exception
        if(! $token = JWTAuth::attempt($credentials))
        {
            return response()->json(['error'=>'invalid credentials'],400);
        }
        return response()->json(compact('token'));
    }

    public function register(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'name' => 'required|string|max:255',
            'email' => 'required|string|unique:users|max:255|email',
            'password' => 'required|string|min:6'
        ]);
        
        if($validator->fails())
        {
            return response()->json($validator->errors(),400);
        }

        $user = User::create([
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'password' => Hash::make($request->get('password'))
        ]);

        $token = JWTAuth::fromUser($user);

        return response()->json(compact('user','token'),201);
    }

}
