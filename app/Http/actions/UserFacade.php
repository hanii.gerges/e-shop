<?php


namespace App\Http\actions;
use Illuminate\Support\Facades\Facade;

class UserFacade extends Facade
{

    public static function getFacadeAccessor()
    {
        return UserCRUD::class;
    }
}